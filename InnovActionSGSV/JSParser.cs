﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Text.RegularExpressions;
using System.IO;

namespace InnovAction.Modules.InnovActionSGSV
{
    public static class JSParser
    {
        //"<script\\s((?<aname>[-\\w]+)=[\"'](?<avalue>.*?)[\"']\\s?)*\\s*>(?<script>.*?)</script>"
        private static readonly Regex REGEX_CLIENTSCRIPTS = new Regex(
            @"(?s)<\s?script.*?(/\s?>|<\s?/\s?script\s?>)",
        RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Compiled |
        RegexOptions.ExplicitCapture);

        private static readonly Regex srcRegex = new Regex(@"(?<=src="").*?(?="")",
        RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Compiled |
        RegexOptions.ExplicitCapture);
        private static readonly Regex scriptRegex = new Regex(@"<\s?script[^/][^>]*>(.*?)",
        RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Compiled |
        RegexOptions.ExplicitCapture);


        public static string PurgeScript(string TheScript)
        {
            TheScript = TheScript.Replace("[Updateable]", "");
            TheScript = TheScript.Replace("[StartingScript]", "");
            TheScript = TheScript.Replace("[EndingScript]", "");
            return  TheScript;
        }

        public static string ParseJS(string htmlsource, Control TheControl)
        {
            if (!string.IsNullOrEmpty(htmlsource) && htmlsource.IndexOf(
                "<script", StringComparison.CurrentCultureIgnoreCase) > -1)
            {
                MatchCollection matches = REGEX_CLIENTSCRIPTS.Matches(htmlsource);
                int tmpID = 0000;
                foreach(var TheMatch in matches){
                    string TheScript = TheMatch.ToString();
                    string scriptID = "";

                    string ScriptOpeningTag = scriptRegex.Match(TheScript).ToString();
                    if (ScriptOpeningTag.Contains("[Updateable]"))
                    {
                      
                        scriptID = Guid.NewGuid().ToString();
                    }
                    else{
                        scriptID = TheControl.ClientID + "scriptID" + tmpID;
                    }
                   

                    var SrcMatch = srcRegex.Match(ScriptOpeningTag.ToString());
                    string scriptSrc = SrcMatch.Value;



                  // string scriptSrc = TheMatch.ToString().match( /<script src="scripts\/(.*?)\.scripts\.js"><\/script>/g )[1]

                     if (!string.IsNullOrEmpty(scriptSrc))
                        {
                
                            System.Web.UI.ScriptManager.RegisterClientScriptInclude(TheControl, TheControl.GetType(), scriptID, scriptSrc);
                    
                        }
                        else
                        {
                            if (ScriptOpeningTag.Contains("[StartingScript]"))
                            {
                              TheScript = PurgeScript(TheScript);
                                 System.Web.UI.ScriptManager.RegisterStartupScript(TheControl, TheControl.GetType(), scriptID, TheScript, false);
                            }
                            else if (ScriptOpeningTag.Contains("[EndingScript]")){
                                TheScript = PurgeScript(TheScript);
                                System.Web.UI.ScriptManager.RegisterClientScriptBlock(TheControl, TheControl.GetType(), scriptID, TheScript, false);
                          
                            }
                            else{
                                TheScript = PurgeScript(TheScript);
                                System.Web.UI.ScriptManager.RegisterClientScriptBlock(TheControl, TheControl.GetType(), scriptID, TheScript, false);
                                }
                         }


                     htmlsource = htmlsource.Replace(TheScript, "");
                     tmpID++;
                }

       
            }
            return htmlsource;
        }

   

    }
}
