﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InnovAction.Modules.InnovActionSGSV
{

    public class InnovActionSGSV_RepeatersRecord : DeadlyDAL.DALClass
    {
        public override string TablePK { get { return "IDRepeater"; } }
        public override string TableName { get { return "InnovActionSGSV_Repeaters"; } }
        public int? IDRepeater;
        public int? ModID;
        public string RepeaterName;
        public string Query;
        public string Template;
        public string SQLConnectionString;
    }

    public class InnovActionSGSV_VariablesRecord : DeadlyDAL.DALClass
    {
        public override string TablePK { get { return "IDVariable"; } }
        public override string TableName { get { return "InnovActionSGSV_Variables"; } }
        public int? IDVariable;
        public string VariableName;
        public string VariableValue;
        public int? ModuleID;
        public int? TabID;
    }
    public class InnovActionSGSV_SQLVariablesRecord
    {
        public string VariableName;
        public string VariableValue;
    }

}