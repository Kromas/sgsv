﻿/*
' Copyright (c) 2013  Christoc.com
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Exceptions;
using System.Collections.Generic;

namespace InnovAction.Modules.InnovActionSGSV
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The Settings class manages Module Settings
    /// 
    /// Typically your settings control would be used to manage settings for your module.
    /// There are two types of settings, ModuleSettings, and TabModuleSettings.
    /// 
    /// ModuleSettings apply to all "copies" of a module on a site, no matter which page the module is on. 
    /// 
    /// TabModuleSettings apply only to the current module on the current page, if you copy that module to
    /// another page the settings are not transferred.
    /// 
    /// If you happen to save both TabModuleSettings and ModuleSettings, TabModuleSettings overrides ModuleSettings.
    /// 
    /// Below we have some examples of how to access these settings but you will need to uncomment to use.
    /// 
    /// Because the control inherits from InnovActionSGSVSettingsBase you have access to any custom properties
    /// defined there, as well as properties from DNN such as PortalId, ModuleId, TabId, UserId and many more.
    /// </summary>
    /// -----------------------------------------------------------------------------
    public partial class Settings : InnovActionSGSVModuleSettingsBase
    {
     
        protected void Page_Load(object sender, EventArgs e)
        {
            tx_modid.Text = "Module ID: " + ModuleId.ToString();

            TextBox1.Text = @"<input id=""Button1"" type=""button"" value=""SendMessage"" onclick=""javascript: [ModuleScript].SendInnerMessage('Testnumber[:]1[/]SecondOption[:]True', '216; 211');"" /> ";



            TextBox2.Text = @"<input id=""Button1"" type=""button"" value=""BroadcastMessage now"" onclick=""javascript: [ModuleScript].BroadCastInnerMessage('Testnumber[:]1[/]SecondOption[:]True');"" /> ";

            TextBox3.Text = @"<input id=""Button1"" type=""button"" value=""CallSP"" onclick=""javascript:[ModuleScript].CallSP('MyTestSP',Testnumber[:]1[/]SecondOption[:]True);"" /> ";
            TextBox4.Text = @"<input id=""Button1"" type=""button"" value=""CallScalarSP"" onclick=""javascript: [ModuleScript].CallScalarSP('MyTestSP',Testnumber[:]1[/]SecondOption[:]True);"" /> ";

            TextBox5.Text = @"<input type=""text"" name=""tx_nombre"" data-sgsv=""Formulario1"" data-sgsv-name=""nombre"">";

            Variable_DynAddControl.ParentMod = this;
            Repeater_DynAddControl.ParentMod = this;
        }



        #region Base Method Implementations

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// LoadSettings loads the settings from the Database and displays them
        /// </summary>
        /// -----------------------------------------------------------------------------
        public override void LoadSettings()
        {
          
            try
            {
                if (!IsPostBack)
                {
                    //Check for existing settings and use those on this page
                    //if (Settings.Contains("QueryHeader"))
                    //{
                    //    tx_HeaderQuery.Text = Settings["QueryHeader"].ToString();
                    //}
                    //if (Settings.Contains("TemplateHeader"))
                    //{
                    //    tx_HeaderTemplate.Text = Settings["TemplateHeader"].ToString();
                    //}

                    if (Settings.Contains("QueryBody"))
                    {
                        tx_BodyQuery.Text = Settings["QueryBody"].ToString();
                    }
                    if (Settings.Contains("TemplateBody"))
                    {
                        tx_BodyTemplate.Text = Settings["TemplateBody"].ToString();
                    }
                    if (Settings.Contains("NullHTML"))
                    {
                        tx_NullRenderHTML.Text = Settings["NullHTML"].ToString();
                    }
                    if (Settings.Contains("SQLConnectionString"))
                    {
                        tx_BodySQLcon.Text = Settings["SQLConnectionString"].ToString();
                    }


                    if (Settings.Contains("SQLVariableConString"))
                    {
                        tx_SQLVariableConString.Text = Settings["SQLVariableConString"].ToString();
                    }

                    if (Settings.Contains("SQLVariableQuery"))
                    {
                        tx_SQLVariableQuery.Text = Settings["SQLVariableQuery"].ToString();
                    }
                     
                    //if (Settings.Contains("QueryFooter"))
                    //{
                    //    tx_FooterQuery.Text = Settings["QueryFooter"].ToString();
                    //}
                    //if (Settings.Contains("TemplateFooter"))
                    //{
                    //    tx_FooterTemplate.Text = Settings["TemplateFooter"].ToString();
                    //}

                    // we load the repeaters
                    var myRepeaterWhere = new DeadlyDAL.DALClass.Where("ModID", ModuleId.ToString());
                    var RepeaterList = DeadlyDAL.Manager.Select<InnovActionSGSV_RepeatersRecord>(myRepeaterWhere);
                    foreach(var rep in RepeaterList){
                      var MyControl = Repeater_DynAddControl.Add(rep);
                     // MyControl.RepeaterRecord = rep;

                    }

                    // we load the variables
                    List<InnovActionSGSV_VariablesRecord> MyVariables = new List<InnovActionSGSV_VariablesRecord>();
                    var myModuleWhere = new DeadlyDAL.DALClass.Where("ModuleID", ModuleId.ToString());
                    var myTabIDWhere = new DeadlyDAL.DALClass.Where("TabID", TabId.ToString());

                    var MyModuleVariables = DeadlyDAL.Manager.Select<InnovActionSGSV_VariablesRecord>(myModuleWhere);
                    var MyPageVariables = DeadlyDAL.Manager.Select<InnovActionSGSV_VariablesRecord>(myTabIDWhere);

                    MyVariables = MyPageVariables;
                    MyVariables.AddRange(MyModuleVariables);




                    foreach (var rep in MyVariables)
                    {
                        var MyControl = Variable_DynAddControl.Add();
                        MyControl.ParentMod = this;
                        MyControl.tx_ID.Text = rep.IDVariable.ToString();
                        MyControl.tx_VariableName.Text = rep.VariableName;
                        MyControl.tx_Value.Text = rep.VariableValue;
                        if ((rep.TabID != null) && (rep.TabID != 0))
                        {
                            MyControl.chk_pageVar.Checked = true;
                        }

                    }
                    if (Settings.Contains("HideIfNull"))
                    {
                        if (Settings["HideIfNull"].ToString() == "0")
                            CheckBoxHideIfNull.Checked = false;
                        else
                            CheckBoxHideIfNull.Checked = true;

                    }
                    if (Settings.Contains("ParseJS"))
                    {
                        if (Settings["ParseJS"].ToString() == "0")
                            chk_ParseJS.Checked = false;
                        else
                            chk_ParseJS.Checked = true;

                    }

                    if (Settings.Contains("ShowQuery"))
                    {
                        if ( Settings["ShowQuery"].ToString() == "0")
                            CheckBoxMostrarQuery.Checked = false;
                        else
                            CheckBoxMostrarQuery.Checked = true;

                    }



                           if (Settings.Contains("KeepVariables"))
                    {
                        if ( Settings["KeepVariables"].ToString() == "0")
                            chk_KeepQueryVariables.Checked = false;
                        else
                            chk_KeepQueryVariables.Checked = true;

                    }


                    
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// UpdateSettings saves the modified settings to the Database
        /// </summary>
        /// -----------------------------------------------------------------------------
        public override void UpdateSettings()
        {
            try
            {
                ModuleController modules = new ModuleController();

                //the following are two sample Module Settings, using the text boxes that are commented out in the ASCX file.
                 //tab module settings
                //modules.UpdateTabModuleSetting(TabModuleId, "QueryHeader", tx_HeaderQuery.Text);
                //modules.UpdateTabModuleSetting(TabModuleId, "TemplateHeader", tx_HeaderTemplate.Text);
                modules.UpdateModuleSetting(ModuleId, "QueryBody", tx_BodyQuery.Text);
                modules.UpdateModuleSetting(ModuleId, "TemplateBody", tx_BodyTemplate.Text);
                modules.UpdateModuleSetting(ModuleId, "NullHTML", tx_NullRenderHTML.Text);
                modules.UpdateModuleSetting(ModuleId, "SQLConnectionString", tx_BodySQLcon.Text);


                modules.UpdateModuleSetting(ModuleId, "SQLVariableConString", tx_SQLVariableConString.Text);
                modules.UpdateModuleSetting(ModuleId, "SQLVariableQuery", tx_SQLVariableQuery.Text);


                 
                
                //modules.UpdateTabModuleSetting(TabModuleId, "QueryFooter", tx_FooterQuery.Text);
                //modules.UpdateTabModuleSetting(TabModuleId, "TemplateFooter", tx_FooterTemplate.Text);
                
                if (CheckBoxHideIfNull.Checked == true)
                    modules.UpdateModuleSetting(ModuleId, "HideIfNull", 1.ToString());
                else
                    modules.UpdateModuleSetting(ModuleId, "HideIfNull", 0.ToString());



                if (chk_ParseJS.Checked == true)
                    modules.UpdateModuleSetting(ModuleId, "ParseJS", 1.ToString());
                else
                    modules.UpdateModuleSetting(ModuleId, "ParseJS", 0.ToString());



                if (CheckBoxMostrarQuery.Checked == true)
                    modules.UpdateModuleSetting(ModuleId, "ShowQuery", 1.ToString());
                else
                    modules.UpdateModuleSetting(ModuleId, "ShowQuery", 0.ToString());



                if (chk_KeepQueryVariables.Checked == true)
                    modules.UpdateModuleSetting(ModuleId, "KeepVariables", 1.ToString());
                else
                    modules.UpdateModuleSetting(ModuleId, "KeepVariables", 0.ToString());


                
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
           

        }

        #endregion



     

     }

}

