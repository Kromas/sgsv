﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Threading;
using DeadlyDAL;

namespace InnovAction.Modules.InnovActionSGSV
{
    public partial class VariableControl : System.Web.UI.UserControl
    {

       // to add stuff to the children control, just add them on the ascx and replace protected in the designer.cs with public
        private DotNetNuke.Entities.Modules.PortalModuleBase _ParentMod;

        public DotNetNuke.Entities.Modules.PortalModuleBase ParentMod
        {
            get { return _ParentMod; }
            set { _ParentMod = value; }
        }

        public bool IsNew {

            get {
                return chk_IsNew.Checked;
            }
            set {
                chk_IsNew.Checked = value;
            }

                           }


      

        protected void Delete_Click(object sender, EventArgs e)
        {
            // we delete from db and hide it from visibility
            var ToDelete = new InnovActionSGSV_VariablesRecord();
            ToDelete.IDVariable = Convert.ToInt32(tx_ID.Text);
            ToDelete.Delete();

            this.Visible = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void Update_Click(object sender, EventArgs e)
        {

            var ToUpdate = new InnovActionSGSV_VariablesRecord();

            ToUpdate.IDVariable = Convert.ToInt32(tx_ID.Text);
            ToUpdate.VariableName = tx_VariableName.Text;
                ToUpdate.VariableValue = tx_Value.Text;
                if (chk_pageVar.Checked)
                {
                    ToUpdate.TabID = ParentMod.TabId;
                    ToUpdate.ModuleID =0;
                }
                else
                {
                    ToUpdate.ModuleID = ParentMod.ModuleId;
                    ToUpdate.TabID = 0;
                }

            ToUpdate.Update();

        }

    }
}