﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Threading;
using DeadlyDAL;

namespace InnovAction.Modules.InnovActionSGSV
{
    public partial class RepeaterControl : System.Web.UI.UserControl
    {

       // to add stuff to the children control, just add them on the ascx and replace protected in the designer.cs with public
        public InnovActionSGSV_RepeatersRecord RepeaterRecord;

        public bool IsNew {

            get {
                return chk_IsNew.Checked;
            }
            set {
                chk_IsNew.Checked = value;
            }

                           }


      

        protected void Delete_Click(object sender, EventArgs e)
        {
            // we delete from db and hide it from visibility
            // we delete from db and hide it from visibility
            var ToDelete = new InnovActionSGSV_RepeatersRecord();
            ToDelete.IDRepeater = Convert.ToInt32(tx_ID.Text);
            ToDelete.Delete();

            this.Visible = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(RepeaterRecord != null){
            tx_ID.Text = RepeaterRecord.IDRepeater.ToString();
            tx_RepeaterName.Text = RepeaterRecord.RepeaterName;
            tx_IDDisplay.Text = "[Repeater:" + RepeaterRecord.IDRepeater.ToString() + "]";
            tx_query.Text = RepeaterRecord.Query;
            tx_Template.Text = RepeaterRecord.Template;
            tx_sqlConn.Text = RepeaterRecord.SQLConnectionString;
            }
        }

        protected void Update_Click(object sender, EventArgs e)
        {

            var ToUpdate = new InnovActionSGSV_RepeatersRecord();

            ToUpdate.IDRepeater = Convert.ToInt32(tx_ID.Text);
            ToUpdate.RepeaterName = tx_RepeaterName.Text;
            ToUpdate.SQLConnectionString = tx_sqlConn.Text;
            ToUpdate.Query = tx_query.Text.Replace("'","''");
            ToUpdate.Template = tx_Template.Text.Replace("'", "''");
            ToUpdate.Update();

        }

    }
}