﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace InnovAction.Modules.InnovActionSGSV
{
    public partial class DynamicRepeater : DotNetNuke.Framework.UserControlBase
    {

        private DotNetNuke.Entities.Modules.PortalModuleBase _ParentMod;

        public DotNetNuke.Entities.Modules.PortalModuleBase ParentMod
        {
            get { return _ParentMod; }
            set { _ParentMod = value; }
        }

        // change here to set up a different prototype
       List<RepeaterControl> ChildControlList = new List<RepeaterControl>();
       string ChildControlURL = "~/desktopmodules/InnovActionSGSV/RepeaterControl.ascx";
       string ChildPrefix = "ChildControl_";
      // public int TabModuleId;

        
       public List<RepeaterControl> GetChildControlList()
       {
           
           var ToReturn = new List<RepeaterControl>();

           foreach (RepeaterControl elControl in ChildControlList)
           {

               //trabajo todos los controles menos los que se usaron para "testear"
               if (((elControl.Visible == true) && (elControl.IsNew)) || ((elControl.Visible == false) && (!elControl.IsNew)))
               {

                   ToReturn.Add(elControl);
               }


           }


           return ToReturn;

       }


        protected void Page_Load(object sender, EventArgs e)
        {
           string fieldCount = tx_Qty.Text;
           int count = string.IsNullOrEmpty(fieldCount) ? 0 : Convert.ToInt32(fieldCount);

     
            if (IsPostBack)
            {
             for (int i = 0; i < count; i++)
                {
                    CreateTextBoxSet(i, DynamicPlaceHolder);
                }
            }    

           


        }


        protected void Page_PreRender(object sender, EventArgs e)
        
        {

         
             

            #region dynamicDiv visibility
            dynamicDiv.Visible = true;
            int count = 0;
    foreach(Control Child in DynamicPlaceHolder.Controls){
    
        if(Child.Visible){
            count++;
        }

    }
            if (count > 0)
            {
                dynamicDiv.Visible = true;
            }
            else
            {
                dynamicDiv.Visible = false;
            }
            #endregion
         

        }


        protected void Add_Click(object sender, EventArgs e)
        {
           
                string fieldCount = tx_Qty.Text;
                int count = string.IsNullOrEmpty(fieldCount) ? 0 : Convert.ToInt32(fieldCount);


                // we add to the db and we draw it
                var ToAdd = new InnovActionSGSV_RepeatersRecord();

                ToAdd.ModID = ParentMod.ModuleId;

                ToAdd.RepeaterName = tx_RepeaterName.Text;
                ToAdd.Query = tx_Query.Text;
                ToAdd.Template = tx_Template.Text;
                ToAdd.SQLConnectionString = tx_SQLConn.Text;    
                ToAdd = ToAdd.Insert() as InnovActionSGSV_RepeatersRecord;
                
                var MyControl = Add(ToAdd);

               // MyControl.RepeaterRecord = ToAdd;
                //MyControl.SelectedDate = DDD_picker.SelectedValue;
                //MyControl.SelectedGender = dd_gender.SelectedValue;
                //MyControl.SelectedKid = count + 1;
        
          
         
        }

        void CreateTextBoxSet(int count, PlaceHolder holder)
        {
            //PlaceHolder MyControl = new PlaceHolder();
            //MyControl.ID = "SubHolder_" + count;

            //for (int i = 0; i < 3; i++)
            //{
            //    TextBox txt1 = new TextBox();
            //    Literal lit1 = new Literal();
            //    txt1.ID = "Literal_" + i + "_" + count;
            //    lit1.ID = "TextBox_" + i + "_" + count;
            //    lit1.Text = "&nbsp;<br/>";
            //    MyControl.Controls.Add(txt1);
            //    MyControl.Controls.Add(lit1);
            //}
            RepeaterControl MyControl = (RepeaterControl)Page.LoadControl(ChildControlURL);

          
            MyControl.ID = ChildPrefix + count;
         
            ChildControlList.Add(MyControl);
            holder.Controls.Add(MyControl);
         
        }

        public RepeaterControl Add(InnovActionSGSV_RepeatersRecord infoData)
        {
            string fieldCount = tx_Qty.Text;
            int count = string.IsNullOrEmpty(fieldCount) ? 0 : Convert.ToInt32(fieldCount);


            RepeaterControl MyControl = (RepeaterControl)Page.LoadControl(ChildControlURL);
            MyControl.RepeaterRecord = infoData;

            MyControl.ID = ChildPrefix + count;
            
            count++;


            ChildControlList.Add(MyControl);
            DynamicPlaceHolder.Controls.Add(MyControl);

            tx_Qty.Text = count.ToString();

            return MyControl;
        }

        protected void showAll_Click(object sender, EventArgs e)
        {
            DynamicPlaceHolder.Controls.Clear();
            // we load the repeaters
         
            var RepeaterList = DeadlyDAL.Manager.GetObjectList<InnovActionSGSV_RepeatersRecord>();
            foreach (var rep in RepeaterList)
            {
                var MyControl = Add(rep);
            
               

            }

        }

        //protected void Add_Click(object sender, EventArgs e)
        //{
        //    //List<RepeaterControl> myList = new List<RepeaterControl>();
        //    ////// redibujo los anteriores
        //    //foreach (RepeaterControl prevControl in DynamicControls.Controls)
        //    //{
        //    //    myList.Add(prevControl);
        //    //}

        //    //foreach (var myCont in myList)
        //    //{
        //    //    DynamicControls.Controls.Add(myCont);
        //    //}

        //    //DynamicControls.Controls.Count;

        //    // agrego el nuevo control

        //    TextBox ToAdd = new TextBox();
        //    ToAdd.ID = "Test_" + DynamicControls.Controls.Count.ToString(); 
        //    DynamicControls.Controls.Add(ToAdd);
        //   // ControlList.Add(ToAdd);

        //    tx_Qty.Text = DynamicControls.Controls.Count.ToString();
            
        //}
    }
}