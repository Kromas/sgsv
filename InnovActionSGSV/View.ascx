﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="InnovAction.Modules.InnovActionSGSV.View" %>



<script type="text/javascript">
function InnovactionSGSV() {

        this.SendInnerMessage = function(message, targetModuleID) {
            $('#<%= tx_Message.ClientID %>').val(message);
            $('#<%= tx_ModuleID.ClientID %>').val(targetModuleID);
           

            document.getElementById('<%= WebCaller.ClientID %>').click();

        }
    //asD
        this.BroadCastInnerMessage = function(message) {
          
            $('#<%= tx_Message.ClientID %>').val(message);
           
            document.getElementById('<%= BroadCastCaller.ClientID %>').click();

        }

        this.CallSP = function (spName, spParameters) {
            $('#<%= tx_SPName.ClientID %>').val(spName);
            $('#<%= tx_SPParameters.ClientID %>').val(spParameters);


                document.getElementById('<%= CallSP.ClientID %>').click();

        }

        this.CallScalarSP = function (spName, spParameters) {
        $('#<%= tx_SPName.ClientID %>').val(spName);
          $('#<%= tx_SPParameters.ClientID %>').val(spParameters);


          document.getElementById('<%= CallScalarSP.ClientID %>').click();

      }

        window.InnovactionSGSV = InnovactionSGSV;
    }

    var Module<%= ModuleId %> = new InnovactionSGSV();

</script>


<asp:UpdatePanel ID="TheUpdatePanel" runat="server" ChildrenAsTriggers="False"  UpdateMode="Conditional">
    <ContentTemplate>



       

<asp:Literal ID="OnNull" runat="server" ></asp:Literal>


<div runat="server" id="MainContainer">
<asp:Literal ID="LiteralBody" runat="server" ></asp:Literal>
<asp:Literal ID="LiteralQuery" runat="server" Visible="False"></asp:Literal>
        </div>


<asp:Literal ID="LiteralException" runat="server" Visible="False"></asp:Literal>




        

             <!-- inner helper stuff -->
        <asp:Button ID="WebCaller" runat="server" OnClick="WebCaller_Click" style="display:none"   />
        <asp:TextBox ID="tx_Keeper" runat="server" style="display:none" ></asp:TextBox>

 </ContentTemplate><Triggers>
    
        <asp:AsyncPostBackTrigger ControlID="BroadCastCaller" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="CallScalarSP" EventName="Click" />

        <asp:AsyncPostBackTrigger ControlID="CallSP" EventName="Click" />
    
    </Triggers>
</asp:UpdatePanel>



        <!-- helper stuff -->

<asp:Button ID="BroadCastCaller" runat="server"  OnClick="BroadCastCaller_Click" style="display:none"   />
<asp:Button ID="CallSP" runat="server"  OnClick="CallSP_Click"  style="display:none"  />
<asp:Button ID="CallScalarSP" runat="server"  OnClick="CallScalarSP_Click"  style="display:none"  />


<asp:TextBox ID="tx_ModuleID" runat="server" style="display:none" ></asp:TextBox>
<asp:TextBox ID="tx_Message" runat="server"  style="display:none"  ></asp:TextBox>
<asp:TextBox ID="tx_SPName" runat="server" style="display:none"></asp:TextBox>
<asp:TextBox ID="tx_SPParameters" runat="server" style="display:none"></asp:TextBox>






