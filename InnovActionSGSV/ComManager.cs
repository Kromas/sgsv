﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InnovAction.Modules.InnovActionSGSV
{
    public  static class ComManager
    {


        public enum MessageMode
        {
            Directed,
            Broadcast

        };

        static int CurrentTabID;
        public static List<View> MyListeners = new List<View>();
        public static void UpdateAllListeners()
        {
            foreach (var TheListener in MyListeners)
            {
                TheListener.RefreshModule();
            }
        }

        public static void SendMessage(MessageMode MyMessageMode, string TheMessage, List<int> ListenerModuleID = null, List<string> ListenerModuleName = null)
        {
            foreach (var TheListener in MyListeners)
            {
                if (MyMessageMode == MessageMode.Directed)
                {
                    // split the listeners and send to each
                    if (ListenerModuleID != null)
                    {
                    foreach (var SomeListener in ListenerModuleID)
                    {
                        if (TheListener.ModuleId == SomeListener)
                    {
                        TheListener.GetMessage(TheMessage);
                    }
                    }
                    foreach (var SomeListener in ListenerModuleName)
                    {
                        try
                        {
                            if (TheListener.ModuleContext.Configuration.ModuleTitle == SomeListener)
                            {
                                TheListener.GetMessage(TheMessage);
                            }
                        }
                        catch { // has no title
                        }
                    }


                    }
                }
                else
                {
                    foreach(var EveryListener in MyListeners){
                        EveryListener.GetMessage(TheMessage);
                    }
                }

            }
            
        }

        private static void AddTriggers(View theview){

            foreach(var SomeListener in MyListeners){

                var Trigger1 = new System.Web.UI.AsyncPostBackTrigger();
                Trigger1.ControlID = theview.WebCaller.UniqueID;
                Trigger1.EventName = "Click";
                SomeListener.TheUpdatePanel.Triggers.Add(Trigger1);

                var Trigger2 = new System.Web.UI.AsyncPostBackTrigger();
                Trigger2.ControlID = theview.CallScalarSP.UniqueID;
                Trigger2.EventName = "Click";
                SomeListener.TheUpdatePanel.Triggers.Add(Trigger2);

                var Trigger3 = new System.Web.UI.AsyncPostBackTrigger();
                Trigger3.ControlID = theview.CallSP.UniqueID;
                Trigger3.EventName = "Click";
                SomeListener.TheUpdatePanel.Triggers.Add(Trigger3);

                var Trigger4 = new System.Web.UI.AsyncPostBackTrigger();
                Trigger4.ControlID = theview.BroadCastCaller.UniqueID;
                Trigger4.EventName = "Click";
                SomeListener.TheUpdatePanel.Triggers.Add(Trigger4);

            }

        }

        public static void AddListener(View MyListener)
        {
            if (CurrentTabID == null)
            {
                MyListeners.Clear();
                CurrentTabID = MyListener.TabId;
            }
            if (CurrentTabID != MyListener.TabId)
            {
                MyListeners.Clear();
                CurrentTabID = MyListener.TabId;
            }


            var ToRemoveList = new List<View>();
            foreach (var theview in MyListeners)
            {
                if (theview.ModuleId == MyListener.ModuleId)
                {

                    //AddTriggers(theview);
                    ToRemoveList.Add(theview);

                }

            }
            foreach (var toremove in ToRemoveList)
            {
                MyListeners.Remove(toremove);
            }
            MyListeners.Add(MyListener);
        }

       
    }

}
