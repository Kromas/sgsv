﻿/*
' Copyright (c) 2013  Christoc.com
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using DotNetNuke.Security;
using DotNetNuke.Services.Search;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web;

namespace InnovAction.Modules.InnovActionSGSV
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The View class displays the content
    /// 
    /// Typically your view control would be used to display content or functionality in your module.
    /// 
    /// View may be the only control you have in your project depending on the complexity of your module
    /// 
    /// Because the control inherits from InnovActionSGSVModuleBase you have access to any custom properties
    /// defined there, as well as properties from DNN such as PortalId, ModuleId, TabId, UserId and many more.
    /// 
    /// </summary>
    /// -----------------------------------------------------------------------------
    public partial class View : InnovActionSGSVModuleBase, IActionable
    {
         #region Page Load region

            /// -----------------------------------------------------------------------------
        /// <summary>
        /// Page_Load runs when the control is loaded
        /// </summary>
        /// -----------------------------------------------------------------------------
        /// 
        List<InnovActionSGSV_VariablesRecord> MyVariables = new List<InnovActionSGSV_VariablesRecord>();
        List<InnovActionSGSV_SQLVariablesRecord> MySQLVariables = new List<InnovActionSGSV_SQLVariablesRecord>();
        Dictionary<string, string> ComDictionary = new Dictionary<string, string>();
        Dictionary<string, string> KeptDictionary = new Dictionary<string, string>();
        
        public void GetMessage(string TheMessage)
        {
            SetKeptVariables(TheMessage);
            ComDictionary = new Dictionary<string, string>();
     // turn the message into dictionary keys with key:value

            foreach (var KeyGroup in TheMessage.Split(new string[] { "[/]" }, StringSplitOptions.None))
            {
                var keyValuePair = KeyGroup.Split(new string[] { "[:]" }, StringSplitOptions.None);
                ComDictionary.Add(keyValuePair[0], keyValuePair[1]);
            }

                //tabmodid.Text = TheMessage;
            RefreshModule();
            // StartSGSVModule();
             //   TheUpdatePanel.Update();
               // Response.Redirect("www.google.com");
         
        }
        void SetKeptVariables(string ToKeep)
        {
            if (Settings.Contains("KeepVariables"))
            {
                if (Settings["KeepVariables"].ToString() == "1")
                {
                    // con cada mensaje nuevo esta reemplazando el viewstate, esto esta bueno por que si quiero mandar un mensaje nuevo para que me resetee todo a default, tengo que poder

                    tx_Keeper.Text = ToKeep;
                }
            }
        }
        void GetKeptVariables()
        {
                if (Settings.Contains("KeepVariables"))
            {
                if (Settings["KeepVariables"].ToString() == "1")
                {
                    try
                    {
                        KeptDictionary = new Dictionary<string, string>();
                        string ToKeep = tx_Keeper.Text;
                        // turn the message into dictionary keys with key:value

                        foreach (var KeyGroup in ToKeep.Split(new string[] { "[/]" }, StringSplitOptions.None))
                        {
                            var keyValuePair = KeyGroup.Split(new string[] { "[:]" }, StringSplitOptions.None);
                            KeptDictionary.Add(keyValuePair[0], keyValuePair[1]);
                        }
                    }
                    catch { }
                   
                }
            }
        }
        public void RefreshModule()
        {
            StartSGSVModule();
            TheUpdatePanel.Update();
        
        }
        void ResetModule()
        {
            try
            {
                var myModuleWhere = new DeadlyDAL.DALClass.Where("ModuleID", ModuleId.ToString());
                var myTabIDWhere = new DeadlyDAL.DALClass.Where("TabID", TabId.ToString());

                var MyModuleVariables = DeadlyDAL.Manager.Select<InnovActionSGSV_VariablesRecord>(myModuleWhere);
                var MyPageVariables = DeadlyDAL.Manager.Select<InnovActionSGSV_VariablesRecord>(myTabIDWhere);

                MyVariables = MyModuleVariables;

                //we create and add the sql variables
                MySQLVariables = GetSQLVariables();
                try
            {
                MyVariables.AddRange(MyPageVariables);
            }
                catch { }
            }
            catch { }
            //reset stuff
                LiteralBody.Text ="";
                LiteralQuery.Text ="";
                LiteralException.Text = "";

            MainContainer.Visible = true;
            OnNull.Text = "";
        }
        private List<InnovActionSGSV_SQLVariablesRecord> GetSQLVariables()
        {
            var ToReturn = new List<InnovActionSGSV_SQLVariablesRecord>();
            // we need to load the sql variables into the return

            // we get the sql from SQLVariableConString and SQLVariableQuery settings

            if (Settings.Contains("SQLVariableQuery"))
            {
                string MySqlString = Settings["SQLVariableQuery"].ToString();
                IDataReader Reader = null;
                if (Settings.Contains("SQLConnectionString"))
                {
                    Reader = GetSQL(MySqlString, Settings["SQLConnectionString"].ToString());
                }
                else
                {
                    Reader = GetSQL(MySqlString);
                }

                if (Reader != null)
                {
                 

                    if (Reader.Read())
                    {
                        
                        //SQLReader.Read();
                        int columnCount = Reader.FieldCount;
                        int i = 0;
                        while (i < columnCount)
                        {
                            var ToAdd = new InnovActionSGSV_SQLVariablesRecord();
                            try
                            {
                                // esto debe conseguir el valor y convertirlo a string
                                //MyData = SQLReader.GetString(i);

                                ToAdd.VariableName = Reader.GetName(i).ToString();
                                ToAdd.VariableValue = Reader.GetValue(i).ToString();
                                ToReturn.Add(ToAdd);

                            }
                            catch
                            {

                            }

                            i++;
                        }


                      
                  
                        // MyTemplate = CombineReaderWithTemplate(Reader, MyTemplate);
                    }
                
                  
                }

                // we check if something was added, else we add empty variables
                if (ToReturn.Count <= 0)
                { //SQLReader.Read();
                    int columnCount = Reader.FieldCount;
                    int i = 0;
                    while (i < columnCount)
                    {
                        var ToAdd = new InnovActionSGSV_SQLVariablesRecord();
                        try
                        {
                            // esto debe conseguir el valor y convertirlo a string
                            //MyData = SQLReader.GetString(i);

                            ToAdd.VariableName = Reader.GetName(i).ToString();
                            ToAdd.VariableValue = "";
                            ToReturn.Add(ToAdd);

                        }
                        catch
                        {

                        }

                        i++;
                    }
                }


            }

            return ToReturn;

        }
        private void StartSGSVModule()
        {
            GetKeptVariables();
            ResetModule();
           

            LiteralQuery.Text = string.Empty;
            LiteralException.Text = string.Empty;
            //RenderCodeLiteral("QueryHeader", "TemplateHeader",LiteralHeader);
            RenderCodeLiteral("QueryBody", "TemplateBody", LiteralBody);
            //   RenderCodeDataList("QueryBody");
            // RenderCodeLiteral("QueryFooter", "TemplateFooter", LiteralFooter);
        }
        override protected void OnInit(EventArgs e)
        {

            base.OnInit(e);
            ComManager.AddListener(this);
        }
        private void Page_Load(object sender, System.EventArgs e)
        {
           

            //DotNetNuke.Framework.JavaScriptLibraries.JavaScript.RequestRegistration(DotNetNuke.Framework.JavaScriptLibraries.CommonJs.jQuery);
            //DotNetNuke.Framework.JavaScriptLibraries.JavaScript.RequestRegistration(DotNetNuke.Framework.JavaScriptLibraries.CommonJs.DnnPlugins);
            //DotNetNuke.UI.Utilities.ClientAPI.RegisterClientReference(this.Page, DotNetNuke.UI.Utilities.ClientAPI.ClientNamespaceReferences.dnn);
            //DotNetNuke.Framework.AJAX.SetScriptManagerProperty(Page, "EnablePageMethods", new object[]{true});
            //DotNetNuke.Framework.AJAX.SetScriptManagerProperty(Me.Form, "EnablePageMethods", New Object() {True})
            Page.MaintainScrollPositionOnPostBack = true;
            //tabmodid.Text = ModuleId.ToString();
            System.Web.UI.ScriptManager.RegisterClientScriptInclude(Page, Page.GetType(), "InnovactionSGSVScripts", "/DesktopModules/InnovactionSGSV/InnovactionSGSV.js");
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "DropdownSupport", "InitDropDownSupport();", true);
                         


            try
            {
                // we try to get our listener
           
            }
            catch { }


            try
            {

                StartSGSVModule();

                
            }
            catch (Exception ex)
            {
                 Excepcion( ex);
            }
        }

        /// <summary>
        /// this method does something awesome
        /// </summary>
        void RenderCodeLiteral(string Query, string Template, System.Web.UI.WebControls.Literal LiteralAux)
        {
            string ToDraw = BuildTemplate(Query, Template);
            // dibujo el template
            Draw_Literal(ToDraw, LiteralAux);
        }
        
     private string BuildTemplate(string Query, string Template){
   

             string MyTemplate = "";
             if (Settings.Contains(Template))
            {
                MyTemplate = Settings[Template].ToString();
                MyTemplate = GetTemplate(MyTemplate);


                if (Settings.Contains(Query))
                {
                    string MySqlString = Settings[Query].ToString();
                    IDataReader Reader = null;
                    if (Settings.Contains("SQLConnectionString"))
                    {
                        Reader = GetSQL(MySqlString, Settings["SQLConnectionString"].ToString());
                    }
                    else{
                        Reader = GetSQL(MySqlString);
                    }
                   

                    if (Reader != null)
                    {
                        if (Reader.Read())
                        {

                            MyTemplate = CombineReaderWithTemplate(Reader, MyTemplate);
                        }
                        else
                        {
                            // we render the Null stuff
                            if (Settings.Contains("NullHTML"))
                            {
                                OnNull.Text = RenderJS(GetTemplate(Settings["NullHTML"].ToString()));
                            }

                            // we hide the main container if needed
                            if ((Settings.Contains("HideIfNull")) && (Settings["HideIfNull"].ToString() == "1"))
                            {
                                MyTemplate = "";
                                MainContainer.Visible = false;
                            }
                            else
                            {
                                // we continue drawing the rest to remove the extra []'s
                                MyTemplate = CombineReaderWithTemplate(Reader, MyTemplate);
                            }
                        }

                    }
                    else
                    {
                        // we render the Null stuff
                        if (Settings.Contains("NullHTML"))
                        {
                            OnNull.Text = RenderJS(GetTemplate(Settings["NullHTML"].ToString()));
                        }

                        // we hide the main container if needed
                        if ((Settings.Contains("HideIfNull")) && (Settings["HideIfNull"].ToString() == "1"))
                        {
                            MyTemplate = "";
                            MainContainer.Visible = false;
                        }
                        else
                        {
                            // we continue drawing the rest to remove the extra []'s
                            MyTemplate = CombineReaderWithTemplate(Reader, MyTemplate);
                        }
                    }
                }
                else
                {
                    AvisoNoHaySqlQuery(Query);
                }

}
             
    return MyTemplate;

}

     string AddRepeaters(string Template)
     {

         string ToReturn = Template;



         var RepeaterList = DeadlyDAL.Manager.GetObjectList<InnovActionSGSV_RepeatersRecord>();

             foreach (var rep in RepeaterList)
             {
                 string ToReplace = "[Repeater:" + rep.IDRepeater.ToString() + "]";
                 string ToReplaceWithName = "[Repeater:" + rep.RepeaterName + "]";
                 string TheRepeaterString = "";
                 // foreach row in the query we must draw a template
                 //rep.Query;
                 if (ToReturn.Contains(ToReplace))
                 {

                     TheRepeaterString = CreateRepeaterToAdd(rep, TheRepeaterString);
                     ToReturn = ToReturn.Replace(ToReplace, TheRepeaterString);
                 }
                 else if (ToReturn.Contains(ToReplaceWithName))
                 {
                     if (!string.IsNullOrEmpty(rep.RepeaterName))
                     {
                     TheRepeaterString = CreateRepeaterToAdd(rep, TheRepeaterString);
                     ToReturn = ToReturn.Replace(ToReplaceWithName, TheRepeaterString);
                     }
                 }


                 //string MyData = CombineReaderWithTemplate(GetSQL(rep.Query), GetTemplate(rep.Template));
              
            
         }
             return ToReturn;
        

     }

     private string CreateRepeaterToAdd(InnovActionSGSV_RepeatersRecord rep, string TheRepeaterString)
     {
         IDataReader MyReader = null;
         if (!string.IsNullOrEmpty(rep.SQLConnectionString))
         {
             MyReader = GetSQL(rep.Query, rep.SQLConnectionString);
         }
         else
         {
             MyReader = GetSQL(rep.Query);
         }

         //para cada linea del idatareader


         while (MyReader.Read())
         {

             //combinar la linea con el template
             string TheTemplate = GetTemplate(rep.Template);
             TheRepeaterString += CombineReaderWithTemplate(MyReader, TheTemplate);

         }
         return TheRepeaterString;
     }
  


        string CombineReaderWithTemplate(IDataReader SQLReader, string Template)
        {
            //gaspa code
            Template = ChangeColumnNamesToNumbers(SQLReader, Template);
            // nik start
            // we change add the variables
            Template = ChangeVariablesWithValues(Template);


            string ToReturn = Template;
            if (SQLReader != null)
            {
                //SQLReader.Read();
            int columnCount = SQLReader.FieldCount;
            int i =0;
            while (i < columnCount)
            {
                string ToReplace = "[Column:" + i.ToString() + "]";
                string MyData = "";
                
                try
                {
                    // esto debe conseguir el valor y convertirlo a string
                    //MyData = SQLReader.GetString(i);
                    MyData = SQLReader.GetValue(i).ToString();
 
                }
                catch {
                   
                }
                ToReturn = ToReturn.Replace(ToReplace, MyData);
                i++;
            }

            }
            return ToReturn;

        }

        private string ChangeVariablesWithValues(string Template)
        {

            foreach (var vari in MyVariables)
            {
                var theName = "[Variable:" + vari.VariableName + "]";
                while (Template.Contains(theName))
                {
                    Template = Template.Replace(theName, vari.VariableValue);

                }

            }


            foreach (var vari in MySQLVariables)
            {

                // we check if the sql variable has the option to get the defautl variable
                var theNameWithDefaultVal = "[SQLVariable:" + vari.VariableName + ":";

                while (Template.Contains(theNameWithDefaultVal))
                {

                    int myindex = Template.IndexOf(theNameWithDefaultVal);
                    myindex += 13; //So we add the "[SQLVariable:" part
                    string Start = Template.Substring(myindex); // ID:5] from misupertabla where hola = [SQLVariable:HOLI:CHAUCHIS]
                    int squaredBracketIndex = Start.IndexOf("]");
                    string KeyAndDefaultValue = Start.Substring(0, squaredBracketIndex); // ID:5
                    try
                    {
                        string Key = KeyAndDefaultValue.Split(':')[0]; //ID
                        string Value = KeyAndDefaultValue.Split(':')[1]; //5
                        if (string.IsNullOrEmpty(vari.VariableValue))
                        {
                            Template = Template.Replace("[SQLVariable:" + Key + ":" + Value + "]", Value);
                        }
                        else
                        {
                            Template = Template.Replace("[SQLVariable:" + Key + ":" + Value + "]", vari.VariableValue);

                        }
                    }
                    catch
                    {

                    }



                }

                
                    // we try to replace the ones without default variables
                    var theName = "[SQLVariable:" + vari.VariableName + "]";
                    while (Template.Contains(theName))
                    {
                        Template = Template.Replace(theName, vari.VariableValue);

                    }


                

            }

            return Template;

        }

        /// <summary>
        /// This AWESOME method changes the tokens that are in the way of [Column:IDJugador] to [Column:0]
        /// </summary>
        /// <param name="SQLReader">The reader which contains the column names&numbers</param>
        /// <param name="Template">The template which contains the tokens</param>
        /// <returns></returns>
        private string ChangeColumnNamesToNumbers(IDataReader SQLReader, string Template)
        {
            string temp = Template;

            //while there are tokens to replace
            while (temp.IndexOf("[Column:") >= 0)
            {
                int myindex = temp.IndexOf("[Column:");
                myindex += 8; //So we add the "[Column:" part
                string start = temp.Substring(myindex); // we position ourselves on the start of the column name/number
                int squaredBracketIndex = start.IndexOf("]");
                string possibleColumnName = start.Substring(0, squaredBracketIndex); // From IDJugador] to IDJugador
                try
                {
                    //Check if it's a number or a name by trying to parse it
                    int noNeed;
                    bool parseSuccessful = int.TryParse(possibleColumnName, out noNeed);

                    //If the parse is not successful, then it's a column name
                    if (!parseSuccessful)
                    {
                        //if we reach here it's a column name, and we want the number
                        string columnName = possibleColumnName; //we know it's a column name already, so might as well change the name
                        int columnNumber = SQLReader.GetOrdinal(columnName);

                        //Find the column NAME and replace it with it's corresponding column NUMBER in the ORIGINAL template
                        string Find = string.Format("[Column:{0}]", columnName);
                        string Replace = string.Format("[Column:{0}]", columnNumber);
                        Template = Template.Replace(Find, Replace);
                    }
                }
                catch (Exception ex)
                {
                    Excepcion(ex);
                }
                finally { temp = temp.Substring(myindex); }
            }
            return Template;
        }

   
        string GetTemplate(string MyTemplate)
        {
            MyTemplate = ReemplazoTags(MyTemplate);
            if (MyTemplate.Trim() != "")
            {
                // we change add the variables
                MyTemplate = ChangeVariablesWithValues(MyTemplate);

                var myDefaultValues = GetDefaultValues(MyTemplate);
                //Reemplazar lo que t enemos:
                MyTemplate = ReemplazoKeys(MyTemplate, myDefaultValues);
                //Reemplazar lo que NO tenemos:
                MyTemplate = ReemplazoDefaultValue(MyTemplate, myDefaultValues);
                //le agregamos los repeaters
                MyTemplate = AddRepeaters(MyTemplate);

            }
          
            return MyTemplate;
        }


        IDataReader GetSQL(string MySqlString, string sConnectionString = null)
        {
            IDataReader ToReturn = null;

            try
            {
                MySqlString = ReemplazoTags(MySqlString);
                if (MySqlString.Trim() != "")
                {
                    //Reemplazamos las variables globales
                    MySqlString = ChangeVariablesWithValues(MySqlString);

                    var myDefaultValues = GetDefaultValues(MySqlString);                   
                    //Reemplazar lo que tenemos:
                    MySqlString = ReemplazoKeys(MySqlString, myDefaultValues);
                    //Reemplazar lo que NO tenemos:
                    MySqlString = ReemplazoDefaultValue(MySqlString, myDefaultValues);
                    //Reemplazamos las variables globales otra vez, por si arman las cosas alrevez
                    MySqlString = ChangeVariablesWithValues(MySqlString);
                    if (string.IsNullOrEmpty(sConnectionString))
                    {
                        ToReturn = DeadlyDAL.Manager.ExecuteQuery(MySqlString);
                    }
                    else
                    {
                        ToReturn = DeadlyDAL.Manager.ExecuteQuery(MySqlString, sConnectionString);
                    }
                }
            }
            catch { }

            return ToReturn;

        }
        /// <summary>
        /// this method does something awesome
        /// </summary>
        void RenderCodeDataList(string Query)
        {

            if (Settings.Contains(Query))
            {

                var mySQL = ReemplazoTags(Query);
                if (mySQL.Trim() != "")
                {

                    var myDefaultValues = GetDefaultValues(mySQL);
                    //Reemplazar lo que tenemos:
                    mySQL = ReemplazoKeys(mySQL, myDefaultValues);
                    //Reemplazar lo que NO tenemos:
                    mySQL = ReemplazoDefaultValue(mySQL, myDefaultValues);
                    // Llamo al SQL
                    //LlamoSQL_Datalits(mySQL);

                }
                else
                    AvisoNoHaySqlQuery(Query);
            }

            else
            {
                AvisoNoHaySqlQuery(Query);
            }

        }

        private void AvisoNoHaySqlQuery(string Query)
        {
            if (IsEditable)
            {
                LiteralException.Visible = true; // prende el literal de Excepcion para mostrarlo
                LiteralException.Text = LiteralException.Text + "<br />Puede Ingresar una query en los settings de " + Query;
            }
        }


        private void Draw_Literal(string ToDraw, System.Web.UI.WebControls.Literal LiteralAux)
        {


            ToDraw = RenderJS(ToDraw);
          
            LiteralAux.Text = ToDraw;
        }

        private string RenderJS(string ToDraw)
        {

            // we replace all the [Script] tokens for the actual call to the JS script
            ToDraw = ToDraw.Replace("[ModuleScript]", "Module" + ModuleId);

            //we register scripts via codebehind, if necesary and settings apply
            if ((Settings.Contains("ParseJS")) && (Settings["ParseJS"].ToString() == "1"))
            {
                ToDraw = JSParser.ParseJS(ToDraw, TheUpdatePanel);
            }
            // we strip away the updateable token in all cases
            ToDraw = JSParser.PurgeScript(ToDraw);

            return ToDraw;
        }

        private void LlamoSQL_Literal(string mySQL, System.Web.UI.WebControls.Literal LiteralAux)
        {
            
            try
            {
                ///////////  Aqui llama al SQL 
                ShowQuery(mySQL);
                var mydatareader = DotNetNuke.Data.DataProvider.Instance().ExecuteSQL(mySQL);
          
                if (mydatareader.Read())
                    LiteralAux.Text = mydatareader.GetString(0);
                else { LiteralAux.Text = ""; }
            }
            catch (Exception ex) // error 
            {
                ShowQuery(mySQL);
                if (IsEditable) // si es administrador muestra el mensaje
                {
                    LiteralException.Visible = true; // prende el literal de Excepcion para mostrarlo
                    LiteralException.Text = LiteralException.Text + "<br />La query es : " + mySQL + "<br />";
                }
                Excepcion(ex);
            }
          
        }

        IEnumerable<IDataRecord> GetFromReader(IDataReader reader)
        {
            while (reader.Read()) yield return reader;
        }

        //private void LlamoSQL_Datalits(string mySQL)
        //{
        //    try
        //    {
        //        ///////////  Aqui llama al SQL 
        //        ShowQuery(mySQL);
        //        var mydatareader = DotNetNuke.Data.DataProvider.Instance().ExecuteSQL(mySQL);

        //            // LiteralAux.Text = mydatareader.GetString(0);
        //        bool UseLiteral = false;
                    
        //            if (Settings.Contains("BodyDirection")) // direccion de repeticion
        //            {
        //                // we check how to display the repetaer, or if a literal is needed
        //                if (Settings["BodyDirection"].ToString() == "H")
        //                    DataListBody.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Horizontal;
        //                else if (Settings["BodyDirection"].ToString() == "V")
        //                    DataListBody.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Vertical;
        //                else if (Settings["BodyDirection"].ToString() == "L")
        //                    UseLiteral = true;
        //            }    

        //            if (Settings.Contains("CantidadColumnas")) // direccion de repeticion
        //                    DataListBody.RepeatColumns= int.Parse(Settings["CantidadColumnas"].ToString());


                    

        //            if (UseLiteral)
        //            {
        //                // we use the literal
        //                while (mydatareader.Read())
        //                {
        //                      LiteralBody.Text += mydatareader.GetString(0); //The 0 stands for "the 0'th column", so the first column of the result.
        //                       //mydatareader.NextResult();
        //                }
        //           }

        //        else{
        //                // we use the repeater
        //            var dt = new System.Data.DataTable();
        //            dt.Load(mydatareader);
        //            DataListBody.DataSource = dt;
        //            DataListBody.DataBind();
               
        //            }
            
        //    }
        //    catch (Exception ex) // error 
        //    {
        //        ShowQuery(mySQL);
        //        if (IsEditable) // si es administrador muestra el mensaje
        //        {
        //            LiteralException.Visible = true; // prende el literal de Excepcion para mostrarlo
        //            LiteralException.Text = LiteralException.Text + "<br />La query es : " + mySQL + "<br />";
        //        }
        //        Excepcion(ex);
        //    }
        //}


        private static string ReemplazoDefaultValue(string mySQL, Dictionary<string, string> myDefaultValues)
        {
            foreach (var pair in myDefaultValues)
            {
                //reemplazo con defaultValue! =)
                mySQL = mySQL.Replace("[Query:" + pair.Key.ToString() + ":" + pair.Value.ToString() + "]", pair.Value.ToString());
            }
            return mySQL;
        }

       
        private string ReemplazoKeys(string someTemplate, Dictionary<string, string> myDefaultValues)
        {

            // we replace with the intercomunication keys
            foreach (var ComKey in ComDictionary) // 
            {

                var QueryStringWithoutInjection = CheckInjection(ComKey.Value);

                someTemplate = someTemplate.Replace("[Query:" + ComKey.Key + "]", QueryStringWithoutInjection);
                try
                {
                    string StringAReemplazar = "[Query:" + ComKey.Key + ":" + myDefaultValues[ComKey.Key] + "]";

                    someTemplate = someTemplate.Replace(StringAReemplazar, QueryStringWithoutInjection);
                }
                catch { }

            }
            if (Settings.Contains("KeepVariables"))
            {
                if (Settings["KeepVariables"].ToString() == "1")
                {
                    // we replace the remaining query with kept variables
                    foreach (var KeptKey in KeptDictionary) // 
                    {

                        var QueryStringWithoutInjection = CheckInjection(KeptKey.Value);

                        someTemplate = someTemplate.Replace("[Query:" + KeptKey.Key + "]", QueryStringWithoutInjection);
                        try
                        {
                            string StringAReemplazar = "[Query:" + KeptKey.Key + ":" + myDefaultValues[KeptKey.Key] + "]";

                            someTemplate = someTemplate.Replace(StringAReemplazar, QueryStringWithoutInjection);
                        }
                        catch { }

                    }
                }

            }



            // we replace with the query string keys
            foreach (var key in HttpContext.Current.Request.QueryString.AllKeys) // 
            {
                var QueryStringWithoutInjection = CheckInjection(HttpContext.Current.Request.QueryString[key]);

                someTemplate = someTemplate.Replace("[Query:" + key + "]", QueryStringWithoutInjection);
                try
                {
                    string StringAReemplazar = "[Query:" + key + ":" + myDefaultValues[key] + "]";

                    someTemplate = someTemplate.Replace(StringAReemplazar, QueryStringWithoutInjection);
                }
                catch {       }

            }
            return someTemplate;
        }

        private string ReemplazoTags(string mySQL)
        {
            

            mySQL = mySQL.Replace("[DNN:UserID]", UserId.ToString());
            mySQL = mySQL.Replace("[DNN:TabID]", TabId.ToString());
            mySQL = mySQL.Replace("[DNN:ModuleID]", ModuleId.ToString());
            mySQL = mySQL.Replace("[DNN:PortalID]", PortalId.ToString());

            try // si está logueado exite user info
            {
                if (UserId != -1) // si está logueado exite user info
                {
                    mySQL = mySQL.Replace("[DNN:Username]", UserInfo.Username.ToString());
                    mySQL = mySQL.Replace("[DNN:DisplayName]", UserInfo.DisplayName.ToString());
                    mySQL = mySQL.Replace("[DNN:Email]", UserInfo.Email.ToString());
                    mySQL = mySQL.Replace("[DNN:LastName]", UserInfo.LastName.ToString());
                    mySQL = mySQL.Replace("[DNN:FirstName]", UserInfo.FirstName.ToString());
                }
            }

            catch (Exception ex) // error en userinfo
            {
                Excepcion(ex);
            }
            return mySQL;
        }

        Dictionary<string, string> GetDefaultValues(string Query)
        {
            Dictionary<string, string> StuffToReplace = new Dictionary<string, string>();

            string temp = Query;
            while (temp.IndexOf("[Query:") >= 0)
            {
                int myindex = temp.IndexOf("[Query:");
                myindex += 7; //So we add the "[Query:" part
                string Start = temp.Substring(myindex); // ID:5] from misupertabla where hola = [Query:HOLI:CHAUCHIS]
                int squaredBracketIndex = Start.IndexOf("]");
                string KeyAndDefaultValue = Start.Substring(0, squaredBracketIndex); // ID:5
                try
                {
                    string Key = KeyAndDefaultValue.Split(':')[0]; //ID
                    string Value = KeyAndDefaultValue.Split(':')[1]; //5
                    StuffToReplace.Add(Key, Value);
                }
                catch 
                {
                   
                }
                finally { temp = temp.Substring(myindex); }
                
            }

            return StuffToReplace;
        }

        #endregion

        #region Injection

        /// <summary>
        /// We remove special characters from querystring
        /// </summary>
        /// <param name="QueryString"></param>
        /// <returns></returns>
        private string CheckInjection(string QueryString)
        {

            // we purge the string from any character other than letters, numbers or spaces
            string ToReturn = System.Text.RegularExpressions.Regex.Replace(QueryString, @"[^a-zA-Z0-9\s]", string.Empty);
            return ToReturn;


            // gaspa old

            //string MyRegex = @"^[a-zA-Z0-9\s]*$";

            //var ValidQueryString = System.Text.RegularExpressions.Regex.IsMatch(MyRegex, QueryString);
            //if (!ValidQueryString)
            //{
            //    QueryString = "";
            //    Exception Ex = new Exception("Intento de Inyección de código maligno en Módulo: " + ModuleId.ToString() + " TabId: " + TabId.ToString());
            //    Excepcigon(Ex);
            //}
            //return QueryString;


            // horacio old

            //string[] blackList = {"--",";","/*","*/","@",
            //                               "char","nchar","varchar","nvarchar",
            //                               "alter","begin","cast","create","cursor","declare","delete","drop","end","exec","execute",
            //                               "fetch","insert","kill","open",
            //                               "select", "sys","sysobjects","syscolumns",
            //                               "table","update","'", ":"};

            //for (int i = 0; i < blackList.Length; i++)
            //{
            //    if ((QueryString.IndexOf(blackList[i].ToLower(), StringComparison.OrdinalIgnoreCase) >= 0))
            //    {
            //       QueryString = "";
            //       Exception Ex = new Exception("Intento de Inyección de código maligno en Módulo: " + ModuleId.ToString() + " TabId: " + TabId.ToString() + "; El QS fue purgado de: " + blackList[i]);
            //       Excepcjion(Ex);
            //    }
                 
            //}
            //return QueryString;
        }

       #endregion


        #region Optional Interfaces

        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection Actions = new ModuleActionCollection();
                //Actions.Add(GetNextActionID(), Localization.GetString("EditModule", this.LocalResourceFile), "", "", "", EditUrl(), false, SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion
        private void Excepcion(Exception ex)
        {
            
            //Exceptions.ProcessModuleLoadException(this, ex);
            if (IsEditable) // si es administrador muestra el mensaje
            {
                LiteralException.Visible = true; // prende el literal de Excepcion para mostrarlo
                LiteralException.Text = LiteralException.Text + "<br />Exception thrown.<br />" + ex.Message;
                if(ex.InnerException != null){
                    LiteralException.Text += "<br />" + ex.InnerException;
                }
            }
            else
                LiteralException.Visible = false;
            
        }

        private void ShowQuery(string Query)
        {
            if (Settings.Contains("ShowQuery"))
            {
                if (Settings["ShowQuery"].ToString() == "1")
                {
                    LiteralQuery.Visible = true; // prende el literal de Excepcion para mostrarlo
                    LiteralQuery.Text =LiteralQuery.Text+ "<br />Query para Debug: " + Query + "<br />";
                }
                else
                    LiteralQuery.Visible = false;
            }
            else
                LiteralQuery.Visible = false; // lo apago por las dudas.
        }

        void datalistwrite()
        {


        }

        protected void DataListBody_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void WebCaller_Click(object sender, EventArgs e)
        {
            //Convert.ToInt32(TextBox1.Text)
            List<string> NewNameListeners = new List<string>();
            List<int> NewListeners = new List<int>();
            foreach (var ListenerID in tx_ModuleID.Text.Split(';'))
            {
                try
                {
                    NewListeners.Add(Convert.ToInt32(ListenerID));
                }
                catch {
                    //support for module names
                    NewNameListeners.Add(ListenerID);
                }
            }

            ComManager.SendMessage(ComManager.MessageMode.Directed, tx_Message.Text, NewListeners, NewNameListeners);
           
        }
        protected void BroadCastCaller_Click(object sender, EventArgs e)
        {
            //Convert.ToInt32(TextBox1.Text)
            ComManager.SendMessage(ComManager.MessageMode.Broadcast, tx_Message.Text);

        }


        protected void CallScalarSP_Click(object sender, EventArgs e)
        {
           
            //string TheSP = "Exec " + tx_SPName.Text + " " ;
            string TheConString = DeadlyDAL.Manager.ConnectionString;
            string TheSPName = tx_SPName.Text;

            var OutParamList = new List<SqlParameter>();

            using (SqlConnection conn = new SqlConnection(TheConString))
            {

                using (SqlCommand cmd = new SqlCommand(TheSPName, conn))
                {
                    OutParamList.Clear();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 2080;
                    try
                    {
                        conn.Open();
                        SqlCommandBuilder.DeriveParameters(cmd);
                        conn.Close();
                    }
                    catch (Exception ex) { }


                    // var SPDictionary = new Dictionary<string, string>();
                    // arrays
                  

                    foreach (var KeyGroup in tx_SPParameters.Text.Split(new string[] { "[/]" }, StringSplitOptions.None))
                    {
                        // cols y valores
                        var keyValuePair = KeyGroup.Split(new string[] { "[:]" }, StringSplitOptions.None);
                        //SPDictionary.Add(keyValuePair[0], keyValuePair[1]);

                        foreach (SqlParameter Param in cmd.Parameters)
                        {
                            if ((Param.Direction == System.Data.ParameterDirection.Output) || (Param.Direction == System.Data.ParameterDirection.InputOutput))
                            {

                                Param.Direction = System.Data.ParameterDirection.Output;
                                OutParamList.Add(Param);
                            }
                            else if (Param.Direction == System.Data.ParameterDirection.ReturnValue)
                            {
                                OutParamList.Add(Param);
                            }
                            if (Param.ParameterName == "@" + keyValuePair[0])
                            {
                                Param.Value = keyValuePair[1];
                            }
                        }

                    }

                    // ....

                    conn.Open();
                    var ReturnValue = cmd.ExecuteScalar();
                    SqlParameter FakeOutParam = new SqlParameter("@SCALAR_VALUE", ReturnValue);
                    OutParamList.Add(FakeOutParam);

                    



                }
            }
            RenderSQLOutput(OutParamList);
            ComManager.UpdateAllListeners();
           


        }
        protected void CallSP_Click(object sender, EventArgs e)
        {
            
                //string TheSP = "Exec " + tx_SPName.Text + " " ;
                string TheConString = DeadlyDAL.Manager.ConnectionString;
                string TheSPName = tx_SPName.Text;

                var OutParamList = new List<SqlParameter>();

                using (SqlConnection conn = new SqlConnection(TheConString))
                {

                    using (SqlCommand cmd = new SqlCommand(TheSPName, conn))
                    {
                        OutParamList.Clear();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 2080;
                        try
                        {
                            conn.Open();
                            SqlCommandBuilder.DeriveParameters(cmd);
                            conn.Close();
                        }
                        catch (Exception ex) { }


                        // var SPDictionary = new Dictionary<string, string>();
                        // arrays
                       

                        foreach (var KeyGroup in tx_SPParameters.Text.Split(new string[] { "[/]" }, StringSplitOptions.None))
                        {
                            // cols y valores
                            var keyValuePair = KeyGroup.Split(new string[] { "[:]" }, StringSplitOptions.None);
                            //SPDictionary.Add(keyValuePair[0], keyValuePair[1]);

                            foreach (SqlParameter Param in cmd.Parameters)
                            {
                                if ((Param.Direction == System.Data.ParameterDirection.Output) || (Param.Direction == System.Data.ParameterDirection.InputOutput))
                                {

                                    Param.Direction = System.Data.ParameterDirection.Output;
                                    OutParamList.Add(Param);
                                }
                                else if (Param.Direction == System.Data.ParameterDirection.ReturnValue)
                                {
                                    OutParamList.Add(Param);
                                }
                                if (Param.ParameterName == "@" + keyValuePair[0])
                                {
                                    Param.Value = keyValuePair[1];
                                }
                            }

                        }

                        // ....

                        conn.Open();
                        cmd.ExecuteNonQuery();


                       

                    }   

                }

                RenderSQLOutput(OutParamList);
                ComManager.UpdateAllListeners();
          

        }

        void RenderSQLOutput (List<SqlParameter> OutParamList){

          

            if(OutParamList.Count > 0){
                var TheScript= "";
                 TheScript+= "<script language='javascript'>";
                // por alguna razon, aveces el sql nos da 5000 RETURN_VALUE
              foreach (SqlParameter Param in OutParamList)
                   {
                  
                     //(Page, Param.ParameterName.Substring(1), Param.Value.ToString(), true);
                     // tx_SPOutput.Text += "function asd(){ var RETURN_VALUE = 10;}";
                       TheScript += "var " + Param.ParameterName.Substring(1) + " = " + Param.Value + "; console.log('Received: " + Param.ParameterName.Substring(1) + " from " + ModuleId + "');";
                   
              }
             TheScript += "</script>";
            //  var sm = System.Web.UI.ScriptManager.GetCurrent(this.Page);
             // var timeout = sm.AsyncPostBackTimeout;
              System.Web.UI.ScriptManager.RegisterStartupScript(TheUpdatePanel,TheUpdatePanel.GetType(), Guid.NewGuid().ToString(), TheScript, false);
             // Page.ClientScript.RegisterClientScriptBlock(TheUpdatePanel.GetType(), Guid.NewGuid().ToString(), TheScript);
            //  Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), TheScript);
            //  DotNetNuke.UI.Utilities.ClientAPI.RegisterStartUpScript(this.Page, TabModuleId + "_outVariables", TheScript);
              
    }
        }

    }
}