﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace InnovAction.Modules.InnovActionSGSV
{
    public partial class DynamicVariables : DotNetNuke.Framework.UserControlBase
    {

        private DotNetNuke.Entities.Modules.PortalModuleBase _ParentMod;

        public DotNetNuke.Entities.Modules.PortalModuleBase ParentMod
        {
            get { return _ParentMod; }
            set { _ParentMod = value; }
        }

        // change here to set up a different prototype
        List<VariableControl> ChildControlList = new List<VariableControl>();
       string ChildControlURL = "~/desktopmodules/InnovActionSGSV/VariableControl.ascx";
       string ChildPrefix = "ChildControl_";
      // public int TabModuleId;


       public List<VariableControl> GetChildControlList()
       {

           var ToReturn = new List<VariableControl>();

           foreach (VariableControl elControl in ChildControlList)
           {

               //trabajo todos los controles menos los que se usaron para "testear"
               if (((elControl.Visible == true) && (elControl.IsNew)) || ((elControl.Visible == false) && (!elControl.IsNew)))
               {

                   ToReturn.Add(elControl);
               }


           }


           return ToReturn;

       }


        protected void Page_Load(object sender, EventArgs e)
        {
           string fieldCount = tx_Qty.Text;
           int count = string.IsNullOrEmpty(fieldCount) ? 0 : Convert.ToInt32(fieldCount);

     
            if (IsPostBack)
            {
             for (int i = 0; i < count; i++)
                {
                    CreateTextBoxSet(i, DynamicPlaceHolder);
                }
            }    

           


        }


        protected void Page_PreRender(object sender, EventArgs e)
        
        {

         
             

            #region dynamicDiv visibility
            dynamicDiv.Visible = true;
            int count = 0;
    foreach(Control Child in DynamicPlaceHolder.Controls){
    
        if(Child.Visible){
            count++;
        }

    }
            if (count > 0)
            {
                dynamicDiv.Visible = true;
            }
            else
            {
                dynamicDiv.Visible = false;
            }
            #endregion
         

        }


        protected void Add_Click(object sender, EventArgs e)
        {
           
                string fieldCount = tx_Qty.Text;
                int count = string.IsNullOrEmpty(fieldCount) ? 0 : Convert.ToInt32(fieldCount);


                // we add to the db and we draw it
                var ToAdd = new InnovActionSGSV_VariablesRecord();

         

                ToAdd.VariableName = tx_VariableName.Text;
                ToAdd.VariableValue = tx_Value.Text;
                if (chk_pageVar.Checked)
                {
                    ToAdd.TabID = ParentMod.TabId;
                    ToAdd.ModuleID = 0;
                }
                else
                {
                    ToAdd.ModuleID = ParentMod.ModuleId;
                    ToAdd.TabID = 0;
                }
                ToAdd = ToAdd.Insert() as InnovActionSGSV_VariablesRecord;
                
                var MyControl = Add();

                // change here to set up a different prototype
                MyControl.ParentMod = ParentMod;

                MyControl.tx_ID.Text = ToAdd.IDVariable.ToString();
                MyControl.tx_VariableName.Text = ToAdd.VariableName;
                MyControl.tx_Value.Text = ToAdd.VariableValue;
                if (chk_pageVar.Checked)
                {
                    MyControl.chk_pageVar.Checked = true;
            }
          
                //MyControl.SelectedDate = DDD_picker.SelectedValue;
                //MyControl.SelectedGender = dd_gender.SelectedValue;
                //MyControl.SelectedKid = count + 1;
        
          
         
        }

        void CreateTextBoxSet(int count, PlaceHolder holder)
        {
            //PlaceHolder MyControl = new PlaceHolder();
            //MyControl.ID = "SubHolder_" + count;

            //for (int i = 0; i < 3; i++)
            //{
            //    TextBox txt1 = new TextBox();
            //    Literal lit1 = new Literal();
            //    txt1.ID = "Literal_" + i + "_" + count;
            //    lit1.ID = "TextBox_" + i + "_" + count;
            //    lit1.Text = "&nbsp;<br/>";
            //    MyControl.Controls.Add(txt1);
            //    MyControl.Controls.Add(lit1);
            //}

            VariableControl MyControl = (VariableControl)Page.LoadControl(ChildControlURL);
            MyControl.ParentMod = ParentMod;
          
            MyControl.ID = ChildPrefix + count;
         
            ChildControlList.Add(MyControl);
            holder.Controls.Add(MyControl);
         
        }

        public VariableControl Add()
        {
            string fieldCount = tx_Qty.Text;
            int count = string.IsNullOrEmpty(fieldCount) ? 0 : Convert.ToInt32(fieldCount);


            VariableControl MyControl = (VariableControl)Page.LoadControl(ChildControlURL);
            MyControl.ParentMod = ParentMod;

            MyControl.ID = ChildPrefix + count;
            
            count++;


            ChildControlList.Add(MyControl);
            DynamicPlaceHolder.Controls.Add(MyControl);

            tx_Qty.Text = count.ToString();

            return MyControl;
        }

        //protected void Add_Click(object sender, EventArgs e)
        //{
        //    //List<RepeaterControl> myList = new List<RepeaterControl>();
        //    ////// redibujo los anteriores
        //    //foreach (RepeaterControl prevControl in DynamicControls.Controls)
        //    //{
        //    //    myList.Add(prevControl);
        //    //}

        //    //foreach (var myCont in myList)
        //    //{
        //    //    DynamicControls.Controls.Add(myCont);
        //    //}

        //    //DynamicControls.Controls.Count;

        //    // agrego el nuevo control

        //    TextBox ToAdd = new TextBox();
        //    ToAdd.ID = "Test_" + DynamicControls.Controls.Count.ToString(); 
        //    DynamicControls.Controls.Add(ToAdd);
        //   // ControlList.Add(ToAdd);

        //    tx_Qty.Text = DynamicControls.Controls.Count.ToString();
            
        //}
    }
}