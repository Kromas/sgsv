﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Settings.ascx.cs" Inherits="InnovAction.Modules.InnovActionSGSV.Settings" %>
<%@ Register TagName="label" TagPrefix="dnn" Src="~/controls/labelcontrol.ascx" %>
<%@ Register Src="DynamicRepeater.ascx" TagPrefix="uc4" TagName="DynamicRepeater" %>
<%@ Register Src="DynamicVariables.ascx" TagPrefix="uc5" TagName="DynamicVariables" %>


<link href="/Resources/Shared/components/CodeEditor/lib/codemirror.css" media="all" type="text/css" rel="stylesheet">
<script src="/Resources/Shared/components/CodeEditor/lib/codemirror.js" type="text/javascript"></script>

<script src="/Resources/Shared/components/CodeEditor/mode/xml/xml.js" type="text/javascript"></script>
<script src="/Resources/Shared/components/CodeEditor/mode/vbscript/vbscript.js" type="text/javascript"></script>
<script src="/Resources/Shared/components/CodeEditor/mode/javascript/javascript.js" type="text/javascript"></script>
<script src="/Resources/Shared/components/CodeEditor/mode/css/css.js" type="text/javascript"></script>
<script src="/Resources/Shared/components/CodeEditor/mode/sql/sql.js" type="text/javascript"></script>
<script src="/Resources/Shared/components/CodeEditor/mode/htmlembedded/htmlembedded.js" type="text/javascript"></script>
<script src="/Resources/Shared/components/CodeEditor/mode/htmlmixed/htmlmixed.js" type="text/javascript"></script>





<style type="text/css">
    .extraFeatures {
    background-color: rgb(240,240,240);
    width:100%;
    display: inline-block;
    }
    .CodeMirror {
    width:100%;
    }
    .level2 {
            padding-left: 4%;
    }
    .level3 {
            padding-left: 8%;
    }
    .level4 {
            padding-left: 12%;
    }
</style>

<div class="dnnForm" id="panels-settings">


        <h2 id="H1" class="dnnFormSectionHead"><a href="#">Module</a></h2>

  <fieldset class="dnnClear">
     <asp:Label ID="tx_modid"  runat="server" Text="" style="font-weight: 700"></asp:Label><br /><br />
<b>Body</b>
      <br />
    <div style="float:left; width:48%">  Query:<br />
 
       <asp:TextBox ID="tx_BodyQuery" runat="server" Height="90px" Width="100%" 
                TextMode="MultiLine" BorderColor="Black"  BorderStyle="Solid" CssClass="NormalTextBox SQLCodingTextbox" /></div>


     <div style="float:left; width:48%"> SQL Connection String (Optional):<br />
 
       <asp:TextBox ID="tx_BodySQLcon" runat="server" Height="90px" Width="100%" 
                TextMode="MultiLine" BorderColor="Black"  BorderStyle="Solid" CssClass="NormalTextBox" /></div>

      <div class="dnnClear"></div>
      <br />
      <div>
Template:<br />
<asp:TextBox ID="tx_BodyTemplate" CssClass="NormalTextBox CodingTextbox"  runat="server" TextMode="MultiLine" Rows="30" Columns="140" />

       <div class="dnnClear"></div>
           </div>

      <br />
      <br />


                


<h2 id="Hssss2" class="dnnFormSectionHead level2"><a href="#">On null render html</a></h2>

  <fieldset class="dnnClear" >
      <em>If the body Query returns null, it will display the following html:</em>
<asp:TextBox ID="tx_NullRenderHTML" CssClass="NormalTextBox CodingTextbox"  runat="server" TextMode="MultiLine" Rows="30" Columns="140" />
  </fieldset>

    <h2 id="Hdfafffff2" class="dnnFormSectionHead level2"><a href="#">Extra features</a></h2>

  <fieldset class="dnnClear" >

         <div class="extraFeatures" > 

    <div style="float:left; width:12%">         <asp:Label ID="Label3" runat="server" Text="Extras:"></asp:Label> </div>
    <div style="float:left; width:84%"> <br />
 <asp:CheckBox ID="CheckBoxMostrarQuery" runat="server"  />
        <asp:Label ID="Label2" runat="server" Text="Mostrar la Query"></asp:Label>
        
        <br />
               <asp:CheckBox ID="CheckBoxHideIfNull" runat="server"  />
         <asp:Label ID="Label1" runat="server" Text="Ocultar si la query es null"></asp:Label>
        <br />  
           <asp:CheckBox ID="chk_ParseJS" runat="server" />
         <asp:Label ID="Label4" runat="server" Text="Registrar los JS en cada postback"></asp:Label>
        <br />  
           <asp:CheckBox ID="chk_KeepQueryVariables" runat="server" />
         <asp:Label ID="Label5" runat="server" Text="Mantener las variables de Query"></asp:Label>
        <br />  
    </div>
</div>
         </fieldset>
     
   </fieldset>
          
              
       








 


  <h2 id="H2" class="dnnFormSectionHead"><a href="#">Globals</a></h2>
      <fieldset class="dnnClear">

  <h2 id="a1dsafasdda" class="dnnFormSectionHead level2"><a href="#">Repeaters</a></h2>

  <fieldset class="dnnClear" >

     <uc4:DynamicRepeater runat="server" id="Repeater_DynAddControl" />
      </fieldset>

       <h2 id="H3" class="dnnFormSectionHead level2"><a href="#">Variables</a></h2>

  <fieldset class="dnnClear">
     <uc5:DynamicVariables runat="server" id="Variable_DynAddControl" />
         </fieldset>


              <h2 id="H5" class="dnnFormSectionHead level2"><a href="#">SQL Variables</a></h2>

  <fieldset class="dnnClear">
 

        <div style="float:left; width:48%">  Query:<br />
 
       <asp:TextBox ID="tx_SQLVariableQuery" runat="server" Height="90px" Width="100%" 
                TextMode="MultiLine" BorderColor="Black"  BorderStyle="Solid" CssClass="NormalTextBox SQLCodingTextbox" /></div>


     <div style="float:left; width:48%"> SQL Connection String (Optional):<br />
 
       <asp:TextBox ID="tx_SQLVariableConString" runat="server" Height="90px" Width="100%" 
                TextMode="MultiLine" BorderColor="Black"  BorderStyle="Solid" CssClass="NormalTextBox" /></div>


         </fieldset>



        <h2 id="H4" class="dnnFormSectionHead level2"><a href="#">Custom Controls</a></h2>

  <fieldset class="dnnClear">
Not implemented yet
         </fieldset>


         </fieldset>


         


          <h2 id="Hadas2" class="dnnFormSectionHead"><a href="#">Ayuda</a></h2>

  <fieldset class="dnnClear">

      <h2 id="ChristopherColumbus" class="dnnFormSectionHead level2"><a href="#">Reemplazo de Tokens</a></h2>

  <fieldset class="dnnClear">
<span class="normal">
    TODO ES CASE SENSITIVE <br />
    
    [Query:Variable]<br />
    [Query:Variable:ValorDefault]<br />                  
    
    [DNN:UserID]<br />
    [DNN:TabID]<br />
    [DNN:ModuleID]<br />
    [DNN:PortalID]<br />
    [DNN:Username]<br />
    [DNN:DisplayName]<br />
    [DNN:Email]<br />
    [DNN:LastName]<br />
    [DNN:FirstName]<br />
    [Repeater:Numero]<br />
    [Repeater:Nombre]<br />
    [Variable:Name]<br />
    [SQLVariable:Name]<br />
    [SQLVariable:Name:DefaultValue]<br />
    [Column:Nombre o Numero de Columna de la query]<br />
    [ModuleScript].NombreDelMetodo (ver  Herramientas JS)<br />
    [Updateable] Token para marcar que un JS debe generar un nuevo ID por cada postback. Es útil para cuando el script se renderiza dinamicamente.<br />
    [StartingScript] Token para marcar que el JS debe Registrarse al inicio de la pagina. Solo funciona con Register JS on each postback seleccionado.<br />
    [EndingScript] Token para marcar que el JS debe Registrarse al final de la pagina. Solo funciona con Register JS on each postback seleccionado.<br />
  
    </span>

         </fieldset>

     

     <h2 id="IsaacNewton" class="dnnFormSectionHead level2"><a href="#">Herramientas JS</a></h2>
    <fieldset class="dnnClear">
             <span  class="normal"><strong>Puedes incluir inputs html llamando a los javascript correspondientes para enviar informacion a otro modulo InnovactionSGSV: </strong> </span>

    <br />
             <strong>[ModuleScript].SendInnerMessage</strong>(Mensaje, TargetModuleId): Envia un mensaje a un modulo a travez del ModuleId:  <br />
    <asp:TextBox ID="TextBox1" width="100%" runat="server"></asp:TextBox>  <br />
             <strong>[ModuleScript].BroadCastInnerMessage</strong>(Mensaje): Envia un mensaje a todos los InnovactionSGSV de la pagina:  <br />
    <asp:TextBox ID="TextBox2"  width="100%" runat="server"></asp:TextBox>
             <strong>[ModuleScript].CallSP</strong>(SPName, Parameter[:]valor[/]Parameter[:]valor): Llama al SP objetivo enviando parametros y sus valores:  <br />
             <em>Devuelve variables JS (RETURN_VALUE y cualquier otra variable out en el sp)</em><br />
    <asp:TextBox ID="TextBox3"  width="100%" runat="server"></asp:TextBox>
             <strong>[ModuleScript].CallScalarSP</strong>(SPName, Parameter[:]valor[/]Parameter[:]valor): Llama al SP objetivo enviando parametros y sus valores:  <br />
             <em>Devuelve variables JS (SCALAR_VALUE, RETURN_VALUE y cualquier otra variable out en el sp)</em><br />
    <asp:TextBox ID="TextBox4"  width="100%" runat="server" ></asp:TextBox>
        <strong>BuildParameters</strong>('GroupName'): Arma el string de parametros para los metodos de llamar SP:  <br />
             <em>Devuelve un string armado con los separadores de parametros, valores, etc ya puestos en funcion a los grupos data-sgsv donde el nombre del parametro es igual a data-sgsv-name </em><br />
    <asp:TextBox ID="TextBox5"  width="100%" runat="server" ></asp:TextBox>
          <strong>SelectedValue=""</strong>: Atributo html para los select:  <br />
             <em>El valor es asignado en cada postback, util para usar por ejemplo: SelectedValue="[Column:1]"</em><br />
   

        

           </fieldset>
         </fieldset>
     <br />
     

    </div>





<script type="text/javascript">
    jQuery(function ($) {
 
       


        var setupModule = function () {
            var TheCodeTextboxes = $(".CodingTextbox");
            var TheSQLTextboxes = $(".SQLCodingTextbox");

            //panels
            $('#panels-settings').dnnPanels();
            $('#panels-settings .dnnFormExpandContent a').dnnExpandAll({
                targetArea: '#panels-settings'

            });
            //html stuff
            var mixedMode = {
                name: "htmlmixed",
                scriptTypes: [{
                    matches: /\/x-handlebars-template|\/x-mustache/i,
                    mode: null
                },
                              {
                                  matches: /(text|application)\/(x-)?vb(a|script)/i,
                                  mode: "vbscript"
                              }]
            };

            $.each(TheCodeTextboxes, function (key, value) {

               var cm = CodeMirror.fromTextArea($(value)[0], {
                    lineNumbers: true,
                    mode: mixedMode,
                    lineWrapping: true,
                    matchBrackets: true,
                    indentUnit: 4,
                    indentWithTabs: true,
                    enterMode: "keep",
                    tabMode: "shift"
               });
     
               cm.on("change", function (cm, change) {
            //       console.log("something changed! (" + change.origin + ")");
                   cm.save();
               });

             
            });


        };

        setupModule();

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {

            // note that this will fire when _any_ UpdatePanel is triggered,
            // which may or may not cause an issue
            setupModule();

        });


    });
</script>   




